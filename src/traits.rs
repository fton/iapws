#[cfg(feature = "ratio")]
use num::{
    BigInt,
    rational::Ratio,
};

pub trait Real: unitage::Real {
    fn mul_add(self, a: &Self, b: &Self) -> Self;
    fn sqrt(self) -> Self;
}

impl Real for f64 {
    fn mul_add(self, a: &Self, b: &Self) -> Self {
        f64::mul_add(self, *a, *b)
    }

    fn sqrt(self) -> Self {
        f64::sqrt(self)
    }
}

#[cfg(feature = "ratio")]
#[cfg_attr(docsrs, doc(cfg(feature = "ratio")))]
impl Real for Ratio<BigInt> {
    fn mul_add(self, a: &Self, b: &Self) -> Self {
        self * a + b
    }

    /// ```text
    /// # use num:: { BigInt, ToPrimitive, rational::Ratio };
    /// # use iapws::traits::Real;
    /// let two = Ratio::new(BigInt::from(2u8), BigInt::from(1u8));
    ///
    /// assert_eq!(two.sqrt().to_f64().unwrap(), 2f64.sqrt());
    /// ```
    fn sqrt(self) -> Self {
        let one = Self::new(1i128.into(), 1i128.into());
        let two = Self::new(2i128.into(), 1i128.into());
        let diff = Self::new(1i128.into(), (!0i128).into());
        let mut approx = self.clone();
        let mut max_limit = 100;

        loop {
            let new_approx = (&approx + (&self / &approx)) / &two;

            if max_limit == 0 || &one - &new_approx / &approx < diff {
                break new_approx;
            }
            approx = new_approx;
            max_limit -= 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use num:: { BigInt, ToPrimitive, rational::Ratio };
    use super::*;

    #[test]
    #[ignore]
    fn test_sqrt() {
        let two = Ratio::new(BigInt::from(2u8), BigInt::from(1u8));

        assert_eq!(two.sqrt().to_f64().unwrap(), 2f64.sqrt());
    }
}