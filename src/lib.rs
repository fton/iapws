#![doc = include_str!("../README.md")]
pub mod traits;
pub mod boundary;
pub mod region1;
pub mod region2;
pub mod region3;
pub mod region4;
pub mod region5;