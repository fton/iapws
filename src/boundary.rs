use crate::traits::Real;
use unitage:: { frac, Frac };

const N1: Frac = frac!(0.348_051_856_289_69e+3);
const N2: Frac = frac!(-0.116_718_598_799_75e+1);
const N3: Frac = frac!(0.101_929_700_393_26e-2);
//const N4: Frac = frac!(0.572_544_598_627_46e+3);
const N4: Frac = N2.neg().div(Frac::from_int(2)).div(N3);
//const N5: Frac = frac!(0.139_188_397_788_70e+2);
const N5: Frac = N1.sub(N2.powi(2).div(Frac::from_int(4)).div(N3));

/// ```
/// # use iapws::boundary::b23_t2p;
/// let left = b23_t2p(623.15);
/// let right = 16.5291643;
/// let diff = 1.0 - left / right;
///
/// assert!(diff < 1e-8, "\n{}\n{}\n{}\n", left, right, diff);
/// ```
pub fn b23_t2p<R: Real>(th: R) -> R {
    R::from_frac(N3)
        .mul_add(&th, &R::from_frac(N2))
        .mul_add(&th, &R::from_frac(N1))
}

/// ```
/// # use iapws::boundary::b23_p2t;
/// assert!(1.0 - b23_p2t(16.5291643) / 623.15 < 1e-10);
/// ```
pub fn b23_p2t<R: Real>(pi: R) -> R {
    let mut result = pi - R::from_frac(N5);

    result /= R::from_frac(N3);
    result = result.sqrt();
    result += R::from_frac(N4);
    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use num::Float;

    #[test]
    fn test_coef_n() {
        let n4 = -N2 / Frac::from_int(2) / N3;
        let left = N4.to_f64().integer_decode();
        let right = n4.to_f64().integer_decode();
        let diff = Frac::from_int(1) - N4 / n4;
        let diff = diff.to_f64();

        assert!(diff < f64::EPSILON, "\n{}\n", diff);
        assert_eq!(left.2, right.2);
        assert_eq!(left.1, right.1);
        //assert!(left.0 ^ right.0 <= 1, "\n{:b}\n{:b}\n", left.0, right.0);

        let four = Frac::from_int(4);
        let _fourf = four.to_f64();
        let n5 = N2.powi(2);
        let _n5f = n5.to_f64();
        let n5 = n5 / four;
        let _n5f = n5.to_f64();
        let n5 = n5 / N3;
        let _n5f = n5.to_f64();
        let n5 = N1 - n5 ;
        let _n5f = n5.to_f64();
        let left = N5.to_f64().integer_decode();
        let right = n5.to_f64().integer_decode();
        let diff = Frac::from_int(1) - N5 / n5;
        let diff = diff.to_f64();

        assert!(diff < 1e-11, "\n{}\n{}\n", diff, n5.to_f64());
        assert_eq!(left.2, right.2);
        assert_eq!(left.1, right.1);
        //assert!(left.0 ^ right.0 <= 1, "\n{:b}\n{:b}\n", left.0, right.0);

    }
}